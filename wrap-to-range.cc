#include "wrap-to.h"

#include <cmath>
#include <functional>

double
WrapToRange(double a, double lowerBound, double upperBound)
{
    double range = upperBound - lowerBound;
    a = std::fmod(a - lowerBound, range);
    if (a < 0)
    {
        a += upperBound;
    }
    else
    {
        a += lowerBound;
    }
    if (a == upperBound)
    {
        a = lowerBound;
    }
    return a;
}

double
WrapTo360FromWrapToRange(double a)
{
    return WrapToRange(a, 0, 360);
}

double
WrapTo180FromWrapToRange(double a)
{
    return WrapToRange(a, -180, 180);
}

double
WrapToPiFromWrapToRange(double a)
{
    return WrapToRange(a, -M_PI, M_PI);
}

WrapToRangeFunction WrapTo360FromWrapToRangeBind =
    std::bind(WrapToRange, std::placeholders::_1, 0, 360);
WrapToRangeFunction WrapTo180FromWrapToRangeBind =
    std::bind(WrapToRange, std::placeholders::_1, -180, 180);
