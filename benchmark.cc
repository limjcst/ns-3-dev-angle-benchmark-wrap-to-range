#include "wrap-to.h"

#include <cassert>
#include <chrono>
#include <cstring>
#include <functional>
#include <iostream>

using BenchmarkFunction = std::function<void(WrapToRangeFunction)>;
constexpr double TOLERANCE = 1e-10;

inline void
BenchmarkWrapToRange(WrapToRangeFunction wrapper, const double lowerBound, const double upperBound)
{
    constexpr double STEP = 0.1;
    const double range = upperBound - lowerBound;

    double x = lowerBound - range;
    // WrapToRange should map [lowerBound - range, upperBound - range) to [lowerBound, upperBound)
    for (; x < lowerBound; x += STEP)
    {
        assert(abs(wrapper(x) - (x + range)) < TOLERANCE);
    }
    // WrapToRange should map [lowerBound, upperBound) to [lowerBound, upperBound)
    for (; x < upperBound; x += STEP)
    {
        assert(abs(wrapper(x) - x) < TOLERANCE);
    }
    // WrapToRange should map [lowerBound + range, upperBound + range) to [lowerBound, upperBound)
    for (; x < upperBound + range; x += STEP)
    {
        assert(abs(wrapper(x) - (x - range)) < TOLERANCE);
    }
}

void
BenchmarkWrapTo180(WrapToRangeFunction wrapper)
{
    BenchmarkWrapToRange(wrapper, -180, 180);
}

void
BenchmarkWrapTo360(WrapToRangeFunction wrapper)
{
    BenchmarkWrapToRange(wrapper, 0, 360);
}

void
BenchmarkWrapToPi(WrapToRangeFunction wrapper)
{
    BenchmarkWrapToRange(wrapper, -M_PI, M_PI);
}

void
TimeBenchmark(BenchmarkFunction benchmark,
              WrapToRangeFunction wrapper,
              std::string name,
              const int repetition = 10000)
{
    // std::cout << "Benchmarking " << name << std::endl;
    std::chrono::high_resolution_clock::time_point start =
        std::chrono::high_resolution_clock::now();
    for (int i = 0; i < repetition; ++i)
    {
        benchmark(wrapper);
    }
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    auto duration = end - start;
    std::cout << "Finish " << name << " after " << (duration.count() / 1e9) << "s" << std::endl;
}

int
main()
{
    TimeBenchmark(BenchmarkWrapTo180, WrapTo180Original, "WrapTo180Original");
    TimeBenchmark(BenchmarkWrapTo180, WrapTo180Fixed, "WrapTo180Fixed");
    TimeBenchmark(BenchmarkWrapTo180, WrapTo180FromWrapToRange, "WrapTo180FromWrapToRange");
    TimeBenchmark(BenchmarkWrapTo180,
                  WrapTo180FromWrapToRangeInline,
                  "WrapTo180FromWrapToRangeInline");
    TimeBenchmark(BenchmarkWrapTo180,
                  WrapTo180FromWrapToRangeMarco,
                  "WrapTo180FromWrapToRangeMarco");
    TimeBenchmark(BenchmarkWrapTo180, WrapTo180FromWrapToRangeBind, "WrapTo180FromWrapToRangeBind");

    TimeBenchmark(BenchmarkWrapTo360, WrapTo360Original, "WrapTo360Original");
    TimeBenchmark(BenchmarkWrapTo360, WrapTo360Fixed, "WrapTo360Fixed");
    TimeBenchmark(BenchmarkWrapTo360, WrapTo360FromWrapToRange, "WrapTo360FromWrapToRange");
    TimeBenchmark(BenchmarkWrapTo360,
                  WrapTo360FromWrapToRangeInline,
                  "WrapTo360FromWrapToRangeInline");
    TimeBenchmark(BenchmarkWrapTo360,
                  WrapTo360FromWrapToRangeMarco,
                  "WrapTo360FromWrapToRangeMarco");
    TimeBenchmark(BenchmarkWrapTo360, WrapTo360FromWrapToRangeBind, "WrapTo360FromWrapToRangeBind");

    constexpr int REPETITION = 100000;
    TimeBenchmark(BenchmarkWrapToPi, WrapToPiOriginal, "WrapToPiOriginal", REPETITION);
    TimeBenchmark(BenchmarkWrapToPi,
                  WrapToPiFromWrapToRange,
                  "WrapToPiFromWrapToRange",
                  REPETITION);
    TimeBenchmark(BenchmarkWrapToPi, TrigonometricWrapToPi, "TrigonometricWrapToPi", REPETITION);
    TimeBenchmark(BenchmarkWrapToPi, WrapToPiFixedPoint, "WrapToPiFixedPoint", REPETITION);

    return 0;
}
