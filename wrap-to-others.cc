#include <cmath>

// From Tommaso Pecorella
// See https://gitlab.com/nsnam/ns-3-dev/-/merge_requests/1445#note_1367681663
double
TrigonometricWrapToPi(double a)
{
    double result = std::atan2(std::sin(a), std::cos(a));
    // result should be less or equal to M_PI
    // the greater-than is because you never know about doubles, rounding, and precision.
    if (result >= M_PI)
        result -= 2 * M_PI;
    return result;
}

// See https://gitlab.com/nsnam/ns-3-dev/-/merge_requests/1445#note_1363591449
double
WrapToPiFixedPoint(double a)
{
    constexpr int64_t INT_RANGE = 100000000000;
    // Divide the input by 2*M_PI.
    // Multiply it by INT_RANGE and store into an integer.
    int64_t b(a / (2 * M_PI) * INT_RANGE);
    // Clamp it between [-INT_RANGE / 2, INT_RANGE / 2)
    b = b % INT_RANGE;
    if (b < -INT_RANGE / 2)
    {
        b += INT_RANGE;
    }
    else if (b >= INT_RANGE / 2)
    {
        b -= INT_RANGE;
    }
    // Divide by INT_RANGE and multiply by 2*M_PI.
    return b * (2 * M_PI) / INT_RANGE;
}
