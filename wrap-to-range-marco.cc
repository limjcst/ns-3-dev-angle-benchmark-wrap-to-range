#include "wrap-to.h"

#include <cmath>

#define WrapToRange(a, lowerBound, upperBound)                                                     \
    double range = upperBound - lowerBound;                                                        \
    a = std::fmod(a - lowerBound, range);                                                          \
    if (a < 0)                                                                                     \
    {                                                                                              \
        a += upperBound;                                                                           \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
        a += lowerBound;                                                                           \
    }                                                                                              \
    if (a == upperBound)                                                                           \
    {                                                                                              \
        a = lowerBound;                                                                            \
    }                                                                                              \
    return a

double
WrapTo360FromWrapToRangeMarco(double a)
{
    WrapToRange(a, 0, 360);
}

double
WrapTo180FromWrapToRangeMarco(double a)
{
    WrapToRange(a, -180, 180);
}
