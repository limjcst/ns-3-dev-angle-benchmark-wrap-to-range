#ifndef WRAP_TO_H
#define WRAP_TO_H
#include <functional>

using WrapToRangeFunction = std::function<double(double)>;

double WrapTo360Original(double a);
double WrapTo360Fixed(double a);
double WrapTo360FromWrapToRange(double a);
double WrapTo360FromWrapToRangeInline(double a);
double WrapTo360FromWrapToRangeMarco(double a);
extern WrapToRangeFunction WrapTo360FromWrapToRangeBind;

double WrapTo180Original(double a);
double WrapTo180Fixed(double a);
double WrapTo180FromWrapToRange(double a);
double WrapTo180FromWrapToRangeInline(double a);
double WrapTo180FromWrapToRangeMarco(double a);
extern WrapToRangeFunction WrapTo180FromWrapToRangeBind;

double WrapToPiOriginal(double a);
double WrapToPiFromWrapToRange(double a);
double TrigonometricWrapToPi(double a);
double WrapToPiFixedPoint(double a);

#endif /* WRAP_TO_H */
