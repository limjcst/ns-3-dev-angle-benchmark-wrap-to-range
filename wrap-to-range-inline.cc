#include "wrap-to.h"

#include <cmath>

inline double
WrapToRange(double a, double lowerBound, double upperBound)
{
    double range = upperBound - lowerBound;
    a = std::fmod(a - lowerBound, range);
    if (a < 0)
    {
        a += upperBound;
    }
    else
    {
        a += lowerBound;
    }
    if (a == upperBound)
    {
        a = lowerBound;
    }
    return a;
}

double
WrapTo360FromWrapToRangeInline(double a)
{
    return WrapToRange(a, 0, 360);
}

double
WrapTo180FromWrapToRangeInline(double a)
{
    return WrapToRange(a, -180, 180);
}
