#include "wrap-to.h"

#include <cmath>

double
WrapTo360Fixed(double a)
{
    a = std::fmod(a, 360);
    if (a < 0)
    {
        a += 360;
    }
    if (a == 360)
    {
        a = 0;
    }

    return a;
}

double
WrapTo180Fixed(double a)
{
    a = std::fmod(a + 180, 360);
    if (a < 0)
    {
        a += 180;
    }
    else
    {
        a -= 180;
    }

    return a;
}
