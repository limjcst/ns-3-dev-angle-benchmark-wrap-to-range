#include "wrap-to.h"

#include <cmath>

double
WrapTo360Original(double a)
{
    a = std::fmod(a, 360);
    if (a < 0)
    {
        a += 360;
    }

    return a;
}

double
WrapTo180Original(double a)
{
    a = std::fmod(a + 180, 360);
    if (a < 0)
    {
        a += 360;
    }
    a -= 180;

    return a;
}

double
WrapToPiOriginal(double a)
{
    a = std::fmod(a + M_PI, 2 * M_PI);
    if (a < 0)
    {
        a += 2 * M_PI;
    }
    a -= M_PI;
    return a;
}
